#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include <math.h>
#include <time.h>

/* XDCtools files */
#include <xdc/std.h>
#include <xdc/runtime/System.h>

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/drivers/PIN.h>
#include <ti/drivers/pin/PINCC26XX.h>
#include <ti/drivers/I2C.h>
#include <ti/drivers/Power.h>
#include <ti/drivers/power/PowerCC26XX.h>
#include <ti/mw/display/Display.h>
#include <ti/mw/display/DisplayExt.h>
#include <ti/drivers/i2c/I2CCC26XX.h>

/* Board Header files */
#include "Board.h"

#include "wireless/comm_lib.h"
#include "wireless/address.h"
#include "sensors/bmp280.h"
#include "sensors/mpu9250.h"
#include "buzzer.h"

/* Task */
#define STACKSIZE 1024
Char mainTaskStack[STACKSIZE];
Char commTaskStack[STACKSIZE];
Char sensorTaskStack[STACKSIZE];
Char rpcTaskStack[STACKSIZE];
Char drawTaskStack[STACKSIZE];
Char buzzTaskStack[STACKSIZE];

// SENSOR ROLE
enum Sensor_Role {
  START = 0,
  COORDINATOR = 1,
  SENSOR = 2,
  CLIENT = 3
};
int SENSOR_ROLE = START;

// DATA ROLE
enum Data_Role {
  TEMPERATURE = 0,
  PRESSURE = 1,
  HUMIDITY = 2,
  MOVEMENT = 3,
  LIGHT = 4
};
int DATA_ROLE = START;



// Muuttujat lampotila, ilmanpaine
double pres, temp;

// JTKJ: Pin configuration and variables here
// JTKJ: Painonappien konfiguraatio ja muuttujat

// Vaihe 1. RTOS:n muuttujat pinnien käyttöön
static PIN_Handle buttonHandle;
static PIN_State buttonState;

static PIN_Handle button1Handle;
static PIN_State button1State;

static PIN_Handle ledHandle;
static PIN_State ledState;

static PIN_Handle buzzerHandle;
static PIN_State buzzerState;

// MPU9250-Sensorin käyttöönotto
static PIN_Handle hMpuPin;
static PIN_State MpuPinState;
static PIN_Config MpuPinConfig[] = {
    Board_MPU_POWER  | PIN_GPIO_OUTPUT_EN | PIN_GPIO_HIGH | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    PIN_TERMINATE
};

// MPU9250 I2C CONFIG
static const I2CCC26XX_I2CPinCfg i2cMPUCfg = {
    .pinSDA = Board_I2C0_SDA1,
    .pinSCL = Board_I2C0_SCL1
};

// Vaihe 2. Pinnien määrittelyt, molemmille pinneille oma konfiguraatio
// Painonappi
PIN_Config buttonConfig[] = {
   Board_BUTTON0  | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_NEGEDGE, // Hox! TAI-operaatio
   PIN_TERMINATE // Määritys lopetetaan aina tähän vakioon
};

// Toiselle buttonille button1Config
PIN_Config button1Config[] = {
   Board_BUTTON1  | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_NEGEDGE, // Hox! TAI-operaatio
   PIN_TERMINATE // Määritys lopetetaan aina tähän vakioon
};

// Ledipinni
PIN_Config ledConfig[] = {
   Board_LED1 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX, 
   PIN_TERMINATE // Määritys lopetetaan aina tähän vakioon
};

PIN_Config buzzerConfig[] = {
    Board_BUZZER | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    PIN_TERMINATE
};

Int main(void) {
    
    initialize();
    
    // Main Task variables
	Task_Handle mainTask;
	Task_Params mainTaskParams;
	
	/* Main Task */
    Task_Params_init(&mainTaskParams);
    mainTaskParams.stackSize = STACKSIZE;
    mainTaskParams.stack = &mainTaskStack;
    mainTaskParams.priority=2;
    
    mainTask = Task_create(mainTaskFxn, &mainTaskParams, NULL);
    if (mainTask == NULL) {
        System_abort("sensorTask create failed!");
    }
    
    // RPC Task variables
    Task_Handle rpcTask;
    Task_Params rpc_TaskParams;
    
    /* RPC Task */
    Task_Params_init(&rpc_TaskParams);
    rpcTaskParams.stackSize = STACKSIZE;
    rpcTaskParams.stack = &rpcTaskStack;
    rpcTaskParams.priority=2;
    
    rpcTask = Task_create(rpcTaskFxn, &rpcTaskParams, NULL);
    if (rpcTask == NULL) {
        System_abort("rpcTask create failed!");
    }
    
    // RPC Task variables
    Task_Handle sensorTask;
    Task_Params sensor_TaskParams;
    
    /* RPC Task */
    Task_Params_init(&sensor_TaskParams);
    sensorTaskParams.stackSize = STACKSIZE;
    sensorTaskParams.stack = &sensorTaskStack;
    sensorTaskParams.priority=2;
    
    sensorTask = Task_create(sensorTaskFxn, &sensorTaskParams, NULL);
    if (sensorTask == NULL) {
        System_abort("rpcTask create failed!");
    }
	
    // Comm-Task variables
	Task_Handle commTask;
	Task_Params commTaskParams;
	
	/* Communication Task */
    Init6LoWPAN(); // This function call before use!

    Task_Params_init(&commTaskParams);
    commTaskParams.stackSize = STACKSIZE;
    commTaskParams.stack = &commTaskStack;
    commTaskParams.priority=1;

    commTask = Task_create(commTaskFxn, &commTaskParams, NULL);
    if (commTask == NULL) {
    	System_abort("commTask create failed!");
    }
	
	// Draw-task variables
	Task_Handle drawTask;
	Task_Params drawTaskParams;
	
	/* Draw Task */
    Task_Params_init(&drawTaskParams);
    drawTaskParams.stackSize = STACKSIZE;
    drawTaskParams.stack = &drawTaskStack;
    drawTaskParams.priority=2;
    
    drawTask = Task_create(drawTaskFxn, &drawTaskParams, NULL);
    if (drawTask == NULL) {
        System_abort("drawTask create failed!");
    }
	
	// Buzz-task variables
	Task_Handle buzzTask;
	Task_Params buzzTaskParams;
	
	/* Buzz Task */
    Task_Params_init(&buzzTaskParams);
    buzzTaskParams.stackSize = STACKSIZE;
    buzzTaskParams.stack = &buzzTaskStack;
    buzzTaskParams.priority=2;
    
    buzzTask = Task_create(buzzTaskFxn, &buzzTaskParams, NULL);
    if (buzzTask == NULL) {
        System_abort("drawTask create failed!");
    }

    // Initialize board
    Board_initGeneral();
    Board_initI2C(); // esimerkista

	// JTKJ: Open and configure the button and led pins here
    
    // Button0
    buttonHandle = PIN_open(&buttonState, buttonConfig);
    if(!buttonHandle) {
       System_abort("Error initializing button pins\n");
    }
    // Button1
    button1Handle = PIN_open(&button1State, button1Config);
    if(!button1Handle) {
       System_abort("Error initializing button pins\n");
    }
    
    ledHandle = PIN_open(&ledState, ledConfig);
    if(!ledHandle) {
       System_abort("Error initializing LED pins\n");
    }
    hMpuPin = PIN_open(&MpuPinState, MpuPinConfig);
    if (hMpuPin == NULL) {
    	System_abort("Pin open failed!");
    }
    buzzerHandle = PIN_open(&buzzerState, buzzerConfig);
    if(!buzzerHandle) {
       System_abort("Error initializing LED pins\n");
    }
    
	// JTKJ: Register the interrupt handler for the button
    // JTKJ: Rekister�i painonapille keskeytyksen k�sittelij�funktio

    if (PIN_registerIntCb(buttonHandle, &buttonFxn) != 0) {
       System_abort("Error registering button callback function");
    }
    if (PIN_registerIntCb(button1Handle, &button1Fxn) != 0) {
       System_abort("Error registering button1 callback function");
    }

    
    /* Start BIOS */
    BIOS_start();

    return (0);
}

void initialize() {
    
}

void send_message(char *msg){
    
    char str[80];

    sprintf(str,"\nSent message: %s", msg);
    System_printf(str);
    System_flush();
    
    //Send6LoWPAN(IEEE80154_SERVER_ADDR, payload, strlen(payload));
    Send6LoWPAN(ADDRESS, msg, strlen(msg));
    int32_t result = StartReceive6LoWPAN();
}

void ack_message(int *m_id){
    
    char ack[5];

    sprintf(ack,"\nACK%d", m_id);
    System_printf(ack);
    System_flush();
    
    //Send6LoWPAN(IEEE80154_SERVER_ADDR, payload, strlen(payload));
    Send6LoWPAN(ADDRESS, ack, strlen(msg));
    int32_t result = StartReceive6LoWPAN();
}

double getTemp(){
    
    // BMP280 OPEN I2C
	i2c = I2C_open(Board_I2C, &i2cParams);
	if (i2c == NULL) {
	    System_abort("Error Initializing I2C\n");
	}
    
    // BMP280 ASK DATA
	bmp280_get_data(&i2c, &pres, &temp);
	
	// BMP280 CLOSE I2C
	I2C_close(i2c);
	
	return temp;
}

double getAirPressure(){
    
    // BMP280 OPEN I2C
	i2c = I2C_open(Board_I2C, &i2cParams);
	if (i2c == NULL) {
	    System_abort("Error Initializing I2C\n");
	}

    // BMP280 ASK DATA
	bmp280_get_data(&i2c, &pres, &temp);
	
	// BMP280 CLOSE I2C
	I2C_close(i2c);
	
	return pres;
    
}

float getMovementData() {
    
    // Euclidian distance
    float ed = 0;
    
    // Accelerometer values: ax,ay,az
    float ax, ay, az;
	ax = ay = az = 0;
	
	// Gyroscope values: gx,gy,gz 	
	float gx, gy, gz;
	gx = gy = gz = 0;
	
    // MPU ASK DATA
	mpu9250_get_data(&i2cMPU, &ax, &ay, &az, &gx, &gy, &gz);
        
    // DO SOMETHING WITH THE DATA / ELEEN TUNNISTUS
    ed = euclidean_distance(ax, ay, az);
        
    if (ed >= 1.25 && currState == IDL) {
        currState = MOVEMENT;
    }
        
    // MPU CLOSE I2C
	I2C_close(i2cMPU);
	
	return ed; // confusing naming
}

float euclidean_distance(float Px, float Py, float Pz) {

    float ed = 0;
    ed = (float) sqrt( (pow(Px, 2) + pow(Py, 2) + pow(Pz, 2)) );
    
    return ed;
}

void buzz(int freq, int sleep_in_milliseconds) {
    
    int sleep;
    sleep = sleep_in_milliseconds * 1000;
    
    buzzerSetFrequency(freq);
    Task_sleep(sleep / Clock_tickPeriod);
    
}

/* ########### TASK FUNCTIONS BEGIN ########### */

/* MAIN TASK */
void mainTaskFxn(UArg arg0, UArg arg1) {
    
    while (1) {
         
         switch (SENSOR_ROLE) {
            
            case START:
                break;
                
            case COORDINATOR:
                break;
                
            case SENSOR:
                break;
                
            case CLIENT:
            
                // Lampotila ja ilmanpaine muuttujiin -> näytölle
                sprintf(dTemp, "Temp: %.1f C", (temp - 32) / 1.8);
                sprintf(dPres, "Pres: %.1f p", pres / 100);
                sprintf(msgTemp, "%.1f C", (temp - 32) / 1.8);
                sprintf(msgPres, "%.1f p", pres / 100);
                break;
        }
        
        	    
	    // WAIT 100MS
    	Task_sleep(50000 / Clock_tickPeriod);
        
    }
   
	// MPU9250 POWER OFF 
	// Because of loop forever, code never goes here
    PIN_setOutputValue(hMpuPin,Board_MPU_POWER, Board_MPU_POWER_OFF);
}

/* RPC Task */
Void rpcTaskFxn(UArg arg0, UArg arg1) {
    
    // Taskiin kaikki coordinator requestit
    
}

/* Communication Task */
Void commTaskFxn(UArg arg0, UArg arg1) {

    // Radio to receive mode
	int32_t result = StartReceive6LoWPAN();
	if(result != true) {
		System_abort("Wireless receive mode failed");
	}
	
	char payload[16]; // viestipuskuri
	uint16_t senderAddr;

    while (1) {

        // If true, we have a message
    	if (GetRXFlag()) {
            
            currState = RECEIVE;
    		// Handle the received message..
    		memset(payload, 0, 16);
    		// Luetaan viesti puskuriin
    		Receive6LoWPAN(&senderAddr, payload, 16);
    		queue_message(payload);
    		System_printf(payload);
    		System_flush();
        }
    	// Absolutely NO Task_sleep in this task!!
    }
}

// voi olla, että movement datalle oma task, i2c-väylä vuorottelu
void sensorTaskFxn(UArg arg0, UArg arg1) {
    
    /* BMP280 SETUP */
    
	I2C_Handle i2c; 
	I2C_Params i2cParams;

    I2C_Params_init(&i2cParams);
    i2cParams.bitRate = I2C_400kHz;
    
    bmp280_setup(&i2c);
    
    /* MOVEMENT DATA SETUP */ 
    
    I2C_Handle i2cMPU; 
	I2C_Params i2cMPUParams;
    
    I2C_Params_init(&i2cMPUParams);
    i2cMPUParams.bitRate = I2C_400kHz;
    i2cMPUParams.custom = (uintptr_t)&i2cMPUCfg;
    
    // MPU OPEN I2C
    i2cMPU = I2C_open(Board_I2C, &i2cMPUParams);
    if (i2cMPU == NULL) {
        System_abort("Error Initializing I2CMPU\n");
	}
    
    // MPU9250 kaynnistys
    PIN_setOutputValue(hMpuPin,Board_MPU_POWER, Board_MPU_POWER_ON);

    // WAIT 100MS FOR THE SENSOR TO POWER UP
	Task_sleep(100000 / Clock_tickPeriod);
    System_printf("MPU9250: Power ON\n");
    System_flush();
    
    // Kalibrointi, minne tama lopulta?
	System_printf("MPU9250: Setup and calibration...\n");
	System_flush();
	
	mpu9250_setup(&i2cMPU);

	System_printf("MPU9250: Setup and calibration OK\n");
	System_flush();
    
    switch (DATA_ROLE) {
        case TEMPERATURE:
            break;
        case PRESSURE:
            break;
        case HUMIDITY:
            break;
        case MOVEMENT:
            break;
    }
    
}

void drawTaskFxn(UArg arg0, UArg arg1) {
    
    
    /* Display */
    Display_Params displayParams;
	displayParams.lineClearMode = DISPLAY_CLEAR_BOTH;
    Display_Params_init(&displayParams);
    
    hDisplay = Display_open(Display_Type_LCD, &displayParams);
    if (hDisplay == NULL) {
        System_abort("Error initializing Display\n");
    }
    
    Display_clear(hDisplay);
    sprintf(dGesture, "Gest: N/A");
    
    while (1) {
        
        if (hDisplay) {
            
        }
    }
}

// Vaihe 3. Napinpainalluksen keskeytyksen käsittelijäfunktio
void buttonFxn(PIN_Handle handle, PIN_Id pinId) {
    
    BTN0_PRESSED = TRUE;
    
    char payload[16];
    
    switch (currMode) {
        case START:
            sprintf(dMsgOut, "o/ @%x", IEEE80154_MY_ADDR);
            currState = TRANSMIT;
            break;
        case MAIN:
        
            // Send temperature / air pressure
            if (!TEMP) {
                TEMP = TRUE;
                sprintf(dMsgOut, "%s @%x", msgTemp, IEEE80154_MY_ADDR);
            }
            else {
                TEMP = FALSE;
                sprintf(dMsgOut, "%s @%x", msgPres, IEEE80154_MY_ADDR);
            }
            currState = TRANSMIT;
            break;
        case MESSAGES:
            set_transmit_mode();
            break;
    }
}

// Vaihe 3. Napinpainalluksen keskeytyksen käsittelijäfunktio
void button1Fxn(PIN_Handle handle, PIN_Id pinId) {
    
    BTN1_PRESSED = TRUE;
}





























