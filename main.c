
#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include <math.h>
#include <time.h>

/* XDCtools files */
#include <xdc/std.h>
#include <xdc/runtime/System.h>

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/drivers/PIN.h>
#include <ti/drivers/pin/PINCC26XX.h>
#include <ti/drivers/I2C.h>
#include <ti/drivers/Power.h>
#include <ti/drivers/power/PowerCC26XX.h>
#include <ti/mw/display/Display.h>
#include <ti/mw/display/DisplayExt.h>
#include <ti/drivers/i2c/I2CCC26XX.h>

/* Board Header files */
#include "Board.h"

#include "wireless/comm_lib.h"
#include "wireless/address.h"
#include "sensors/bmp280.h"
#include "sensors/mpu9250.h"
#include "sensors/opt3001.h"
#include "buzzer.h"

/* Task */
#define STACKSIZE 2048
Char commTaskStack[STACKSIZE];
Char sensorTaskStack[STACKSIZE];
Char drawTaskStack[STACKSIZE];
Char buzzTaskStack[STACKSIZE];

void initialize();
float euclidean_distance(float Px, float Py, float Pz);
void identify_gesture(float ax, float ay, float az);
void queue_message(char []);
void send_gesture_i2c();
void buzz_receive_msg();
void buzz_noah(bool);
void buzz(int, int);
void change_mode();
void set_transmit_mode();

/* Display */
static Display_Handle hDisplay;

// HDisplay muuttujat
char addr[16];
char dTemp[24];
char msgTemp[24];
char msgPres[24];
char dPres[24];
char dGestureLabel[16];
char dGesture[16];
char dMsgOut[16];
char dStart[16];
char dLux[16];
char dMessages[16];
char dTransmitMode[16];
char dDescription[6][16];

// Received message
uint16_t ADDRESS;
char RCVD_MSG[3][16];
char MSG_OUT[16];
char MSG_QUEUE[3][16];

// Booleans
bool BTN0_PRESSED;
bool BTN1_PRESSED;
bool TEMP;
bool SOUND_PLAYING;

// State-machine
enum State {
    IDL = 0,
    MOVEMENT = 1,
    HANDSHAKE = 2,
    HIGH_FIVE = 3,
    RECEIVE = 4,
    TRANSMIT = 5,
    DISPLAY = 6
};
enum State currState = IDL;

enum Mode {
  START = 0,
  MAIN = 1,
  MESSAGES = 2,
  LUX = 3
};
int currMode = START;

enum TransmitMode {
    SERVER = 0,
    BROADCAST = 1
};

int currTransmitMode;

// JTKJ: Pin configuration and variables here
// JTKJ: Painonappien konfiguraatio ja muuttujat

// Vaihe 1. RTOS:n muuttujat pinnien käyttöön
static PIN_Handle buttonHandle;
static PIN_State buttonState;

static PIN_Handle button1Handle;
static PIN_State button1State;

static PIN_Handle ledHandle;
static PIN_State ledState;

static PIN_Handle buzzerHandle;
static PIN_State buzzerState;

// MPU9250-Sensorin käyttöönotto
static PIN_Handle hMpuPin;
static PIN_State MpuPinState;
static PIN_Config MpuPinConfig[] = {
    Board_MPU_POWER  | PIN_GPIO_OUTPUT_EN | PIN_GPIO_HIGH | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    PIN_TERMINATE
};

// MPU9250 I2C CONFIG
static const I2CCC26XX_I2CPinCfg i2cMPUCfg = {
    .pinSDA = Board_I2C0_SDA1,
    .pinSCL = Board_I2C0_SCL1
};

// Vaihe 2. Pinnien määrittelyt, molemmille pinneille oma konfiguraatio
// Painonappi
PIN_Config buttonConfig[] = {
   Board_BUTTON0  | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_NEGEDGE, // Hox! TAI-operaatio
   PIN_TERMINATE // Määritys lopetetaan aina tähän vakioon
};

// Toiselle buttonille button1Config
PIN_Config button1Config[] = {
   Board_BUTTON1  | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_NEGEDGE, // Hox! TAI-operaatio
   PIN_TERMINATE // Määritys lopetetaan aina tähän vakioon
};

// Ledipinni
PIN_Config ledConfig[] = {
   Board_LED1 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX, 
   PIN_TERMINATE // Määritys lopetetaan aina tähän vakioon
};

PIN_Config buzzerConfig[] = {
    Board_BUZZER | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    PIN_TERMINATE
};

void initialize() {
    
    // Button state
    BTN0_PRESSED = FALSE;
    BTN1_PRESSED = FALSE;
    
    // FLAGS
    TEMP = FALSE;
    SOUND_PLAYING = FALSE;
    
    // Init transmission variables
    currTransmitMode = SERVER;
    sprintf(dTransmitMode, "@SERVER:");
    ADDRESS = IEEE80154_SERVER_ADDR;
    
    // Init global variables
    int i;
    for (i = 0; i < 3; i++) {
        sprintf(RCVD_MSG[i], "R#%d: ", i + 1);
    }
    
    // SET MODE START
    currMode = START;
    
    // MODE START
    sprintf(dStart, "   WELCOME");
    sprintf(dDescription[0], "Use buttons to");
    sprintf(dDescription[1], "send messages");
    sprintf(dDescription[2], "and navigate");
    sprintf(dDescription[3], "modes.");
    sprintf(dDescription[4], "Btn ^: ACTION");
    sprintf(dDescription[5], "Btn v: MODE");
    
    // MODE MAIN
    sprintf(addr, "  DEVICE#%3X", IEEE80154_MY_ADDR);
    sprintf(dTemp, "Temp: N/A");
    sprintf(dPres, "Pres: N/A");
    sprintf(dLux, "Lux: N/A");
    sprintf(dGesture, "Gest: N/A");
    
   // MODE MESSAGES
    sprintf(dMessages, "   MESSAGES");
    strcpy(dMsgOut, "N/A");
    
    // MODE LUX, DOES NOT EXIST YET
    sprintf(dLux, "    L U X ");

}

// Vaihe 3. Napinpainalluksen keskeytyksen käsittelijäfunktio
void buttonFxn(PIN_Handle handle, PIN_Id pinId) {
    
    BTN0_PRESSED = TRUE;
    
    char payload[16];
    
    switch (currMode) {
        case START:
            sprintf(dMsgOut, "o/ @%x", IEEE80154_MY_ADDR);
            currState = TRANSMIT;
            break;
        case MAIN:
        
            // Send temperature / air pressure
            if (!TEMP) {
                TEMP = TRUE;
                sprintf(dMsgOut, "%s @%x", msgTemp, IEEE80154_MY_ADDR);
            }
            else {
                TEMP = FALSE;
                sprintf(dMsgOut, "%s @%x", msgPres, IEEE80154_MY_ADDR);
            }
            currState = TRANSMIT;
            break;
        case MESSAGES:
            set_transmit_mode();
            break;
    }
}

// Vaihe 3. Napinpainalluksen keskeytyksen käsittelijäfunktio
void button1Fxn(PIN_Handle handle, PIN_Id pinId) {
    
    BTN1_PRESSED = TRUE;
}


void buzzTaskFxn(UArg arg0, UArg arg1) {
    
    // Jääkö viestiääni tekemättä, jos sleep sattuu RECEIVE-tilan kohdalle
    
    while (1) {
        
        if (!SOUND_PLAYING) {
            
            if (currState == RECEIVE) {
                SOUND_PLAYING = TRUE;
                buzz_receive_msg();
                currState = DISPLAY;
                SOUND_PLAYING = FALSE;
            }
            
            if (currMode == START && BTN0_PRESSED) {
                BTN0_PRESSED = FALSE;
                SOUND_PLAYING = TRUE;
                buzz_noah(true);
                SOUND_PLAYING = FALSE;
            }
        }
        Task_sleep(100000 / Clock_tickPeriod);
    }
}

/* Communication Task */
Void commTaskFxn(UArg arg0, UArg arg1) {

    // Radio to receive mode
	int32_t result = StartReceive6LoWPAN();
	if(result != true) {
		System_abort("Wireless receive mode failed");
	}
	
	char payload[16]; // viestipuskuri
	uint16_t senderAddr;

    while (1) {

        // If true, we have a message
    	if (GetRXFlag()) {
            
            currState = RECEIVE;
    		// Handle the received message..
    		memset(payload, 0, 16);
    		// Luetaan viesti puskuriin
    		Receive6LoWPAN(&senderAddr, payload, 16);
    		queue_message(payload);
    		System_printf(payload);
    		System_flush();
        }
    	// Absolutely NO Task_sleep in this task!!
    }
}

void drawTaskFxn(UArg arg0, UArg arg1) {
    
    /* Display */
    Display_Params displayParams;
	displayParams.lineClearMode = DISPLAY_CLEAR_BOTH;
    Display_Params_init(&displayParams);
    
    hDisplay = Display_open(Display_Type_LCD, &displayParams);
    if (hDisplay == NULL) {
        System_abort("Error initializing Display\n");
    }
    
    Display_clear(hDisplay);
    sprintf(dGesture, "Gest: N/A");
    
    while (1) {
        
        if (BTN1_PRESSED) {
            change_mode();
            BTN1_PRESSED = FALSE;
        }
        
        if (hDisplay) {
            
            // Grafiikkaa varten tarvitsemme lisää RTOS:n muuttujia
            tContext *pContext = DisplayExt_getGrlibContext(hDisplay);
            Display_clear(hDisplay);
            
            switch (currMode) {
                case START:
                    if (pContext) {
                        // Piirretään puskuriin kaksi linjaa näytön poikki x:n muotoon
                        GrLineDraw(pContext, 0, 0, 95, 0);
                        Display_print0(hDisplay, 1, 1, addr);
                        GrLineDraw(pContext, 1, 20, 95, 20);
                        Display_print0(hDisplay, 3, 1, dStart);
                        
                        Display_print0(hDisplay, 5, 1, dDescription[0]);
                        Display_print0(hDisplay, 6, 1, dDescription[1]);
                        Display_print0(hDisplay, 7, 1, dDescription[2]);
                        Display_print0(hDisplay, 8, 1, dDescription[3]);
                        Display_print0(hDisplay, 10, 1, dDescription[4]);
                        Display_print0(hDisplay, 11, 1, dDescription[5]);
                        
                        
                        GrLineDraw(pContext, 0, 0, 0, 96);
                        GrLineDraw(pContext, 95, 0, 95, 95);
                        GrLineDraw(pContext, 0, 95, 95, 95);
        
                        // Piirto puskurista näytölle
                        GrFlush(pContext);
                    }
                    break;
                case MAIN:
                    if (pContext) {
                        // Piirretään puskuriin kaksi linjaa näytön poikki x:n muotoon
                        GrLineDraw(pContext, 0, 0, 95, 0);
                        Display_print0(hDisplay, 1, 1, addr);
                        
                        GrLineDraw(pContext, 1, 20, 95, 20);
                        Display_print0(hDisplay, 3, 1, dTemp);
                        Display_print0(hDisplay, 4, 1, dPres);
                        Display_print0(hDisplay, 5, 1, dLux); // new
                        
                        GrLineDraw(pContext, 1, 52, 95, 52);
                        //GrLineDraw(pContext, 1, 42, 95, 42); // original
                        Display_print0(hDisplay, 7, 1, dGesture);
                        
                        Display_print0(hDisplay, 9, 1, dTransmitMode);
                        Display_print0(hDisplay, 10, 2, dMsgOut);
                        
                        GrLineDraw(pContext, 0, 0, 0, 96);
                        GrLineDraw(pContext, 95, 0, 95, 95);
                        GrLineDraw(pContext, 0, 95, 95, 95);
        
                        // Piirto puskurista näytölle
                        GrFlush(pContext);
                    }
                    break;
                case MESSAGES:
                    if (pContext) {
                        // Piirretään puskuriin kaksi linjaa näytön poikki x:n muotoon
                        GrLineDraw(pContext, 0, 0, 95, 0);
                        Display_print0(hDisplay, 1, 1, addr);
                        
                        GrLineDraw(pContext, 1, 20, 95, 20);
                        Display_print0(hDisplay, 3, 1, dMessages);
                        Display_print0(hDisplay, 5, 1, RCVD_MSG[0]);
                        Display_print0(hDisplay, 6, 1, RCVD_MSG[1]);
                        Display_print0(hDisplay, 7, 1, RCVD_MSG[2]);
                        
                        Display_print0(hDisplay, 9, 0, dTransmitMode);
                        Display_print0(hDisplay, 10, 1, dMsgOut);
                        
                        //GrLineDraw(pContext, 1, 42, 95, 42);
                        GrLineDraw(pContext, 0, 0, 0, 96);
                        GrLineDraw(pContext, 95, 0, 95, 95);
                        GrLineDraw(pContext, 0, 95, 95, 95);
        
                        // Piirto puskurista näytölle
                        GrFlush(pContext);
                    }
                    break;
            }
        }
        Task_sleep(1000000 / Clock_tickPeriod);
    }
    
}

void sensorTaskFxn(UArg arg0, UArg arg1) {
    
    // Alustus BMP280 & OPT3001
	I2C_Handle i2c; 
	I2C_Params i2cParams;
	
	// Alustus MPU9250
	I2C_Handle i2cMPU; 
	I2C_Params i2cMPUParams;
    
    // BMP280 & OPT3001
    I2C_Params_init(&i2cParams);
    i2cParams.bitRate = I2C_400kHz;

    // MPU9250
    I2C_Params_init(&i2cMPUParams);
    i2cMPUParams.bitRate = I2C_400kHz;
    i2cMPUParams.custom = (uintptr_t)&i2cMPUCfg;
    
    // Avataan MPU I2C
    i2cMPU = I2C_open(Board_I2C, &i2cMPUParams);
    if (i2cMPU == NULL) {
        System_abort("Error Initializing I2CMPU\n");
    }
    
    // MPU9250 kaynnistys
    PIN_setOutputValue(hMpuPin,Board_MPU_POWER, Board_MPU_POWER_ON);

    // WAIT 100MS FOR THE SENSOR TO POWER UP
	Task_sleep(100000 / Clock_tickPeriod);
    System_printf("MPU9250: Power ON\n");
    System_flush();
    
    // Kalibrointi
	System_printf("MPU9250: Setup and calibration...\n");
	System_flush();

	mpu9250_setup(&i2cMPU);

    // MPU CLOSE I2C
    I2C_close(i2cMPU);

    // BMP280 OPEN I2C
    i2c = I2C_open(Board_I2C, &i2cParams);
    if (i2c == NULL) {
        System_abort("Error Initializing I2C\n");
    }

    // BMP280 SETUP
    bmp280_setup(&i2c);
    
    // OPT3001 SETUP
    opt3001_setup(&i2c);
    
    // CLOSE I2C BMP & OPT
    I2C_close(i2c);
    
    // Kiihtyvyyden tulostaminen
    char dAc[120];
    
    // Muuttujat lampotila, ilmanpaine, valoisuus
    double pres, temp, lux;
    
    // Accelerometer values: ax,ay,az
    float ax, ay, az;
	ax = ay = az = 0;
	
	// Gyroscope values: gx,gy,gz 	
	float gx, gy, gz;
	gx = gy = gz = 0;
    
    float ed = 0;
    
    
    // LOOP FOREVER    
	while (1) {

        // BMP280 OPEN I2C
	    i2c = I2C_open(Board_I2C, &i2cParams);
	    if (i2c == NULL) {
	        System_abort("Error Initializing I2C\n");
	    }

	    // BMP280 ASK DATA
	    bmp280_get_data(&i2c, &pres, &temp);
	    
	    // OPT3001 ASK DATA
	    opt3001_get_data(&i2c, &lux);
        
        // Lampotila ja ilmanpaine muuttujiin -> näytölle
        char fTemp[16], fPres[16], fLux[16];
        
        sprintf(fTemp, "%5s %4.1f %c", "Temp:", (temp - 32) / 1.8, 'C');
        sprintf(fPres, "%5s %4.f %c", "Pres:", pres / 100, 'p');
        sprintf(fLux, "%5s %4.f %c", "Luxs:", lux, 'l');
        
        sprintf(dTemp, "%s", fTemp);
        sprintf(dPres, "%s", fPres);
        sprintf(dLux, "%s", fLux);
        sprintf(msgTemp, "%.1f C", (temp - 32) / 1.8);
        sprintf(msgPres, "%.1f p", pres / 100);
    
        // DEBUG
        //System_printf(dLux);
        //System_flush();
        
	    // BMP280 CLOSE I2C
	    I2C_close(i2c);

	    // MPU OPEN I2C
	    i2cMPU = I2C_open(Board_I2C, &i2cMPUParams);
	    if (i2cMPU == NULL) {
	        System_abort("Error Initializing I2CMPU\n");
	    }
        
        switch (currState) {
            case IDL:
                sprintf(dGesture, "Gest: N/A");
                break;
            case MOVEMENT:
                identify_gesture(ax, ay, az);
                if (ed < 1.25) {
                    currState = IDL;
                }
                break;
            case HIGH_FIVE:
                sprintf(dMsgOut, "o/{SLAP}\\o");
                sprintf(dGesture, "G: o/{SLAP}\\o");
                System_printf(dMsgOut);
                System_flush();
                currState = TRANSMIT;
                break;
            case HANDSHAKE:
                sprintf(dMsgOut, "o_{MOI}_o");
                sprintf(dGesture, "G: o_{MOI}_o");
                System_printf(dMsgOut);
                System_flush();
                currState = TRANSMIT;
                break;
            case TRANSMIT:
                send_gesture_i2c(dMsgOut);
                Task_sleep(100000 / Clock_tickPeriod);
                currState = DISPLAY;
                break;
            case DISPLAY:
                Task_sleep(1500000 / Clock_tickPeriod);
                currState = IDL;
                break;
        }
        
        // MPU ASK DATA
		mpu9250_get_data(&i2cMPU, &ax, &ay, &az, &gx, &gy, &gz);
        
        // DO SOMETHING WITH THE DATA / ELEEN TUNNISTUS
        ed = euclidean_distance(ax, ay, az);
        
        if (ed >= 1.25 && currState == IDL) {
            currState = MOVEMENT;
        }
        
        if (ed > 1.3) {
            sprintf(dAc, "\n-MOVEMENT-\nx: %.2f\ny: %.2f\nz: %.2f\nED: %lf",  ax, ay, az, ed);
            System_printf(dAc);
            System_flush();                    
        }
	    // MPU CLOSE I2C
	    I2C_close(i2cMPU);
	    
	    // WAIT 100MS
    	Task_sleep(50000 / Clock_tickPeriod);
    	
	}
	// MPU9250 POWER OFF 
	// Because of loop forever, code never goes here
    PIN_setOutputValue(hMpuPin,Board_MPU_POWER, Board_MPU_POWER_OFF);
}

// JTKJ: Write the interrupt handler for the button and implement its functionality
// JTKJ: Laadi painonapille keskeytyksen k�sittelij�funktio ja toteudu sille vaadittu toiminnallisuus

Int main(void) {
    
    // TODO
    // - Tarkista musiikki
    // - LUX
    // - LED VALO PALAMAAN VIESTIN MERKIKSI
    // - Modulaarisuuden ehostaminen
    // - Tilakoneen selkeyttäminen
    // - Koodin siistiminen
    
    // LISAOMINAISUUDET:
    // - MENU-rakenne                   -- TEHTY            (1p)
    // - Erilaisia viesteja             -- TEHTY            (1p) 
    // - Ääni viestin vastaanotossa     -- TEHTY            (1p)
    // - Led-valo viestinlähetys        -- TEHTY            (1p) 
    // - Grafiikkaa                     -- TEHTY            (2p)
    // - Animoitua grafiikkaa           --                  (2p)
    // - Musiikkia                      -- TEHTY            (2p)
    // - Dataa muista sensoreista       -- TEHTY            (2p)
    // - Kolme eleentunnistusta         --                  (3p)
    
    initialize();
    
    // Harjoitustyo
	Task_Handle sensorTask;
	Task_Params sensorTaskParams;
	
    // Comm-Task variables
	Task_Handle commTask;
	Task_Params commTaskParams;
	
	// Draw-task variables
	Task_Handle drawTask;
	Task_Params drawTaskParams;
	
	// Buzz-task variables
	Task_Handle buzzTask;
	Task_Params buzzTaskParams;

    // Initialize board
    Board_initGeneral();
    Board_initI2C(); // esimerkista

	// JTKJ: Open and configure the button and led pins here
    
    // Button0
    buttonHandle = PIN_open(&buttonState, buttonConfig);
    if(!buttonHandle) {
       System_abort("Error initializing button pins\n");
    }
    // Button1
    button1Handle = PIN_open(&button1State, button1Config);
    if(!button1Handle) {
       System_abort("Error initializing button pins\n");
    }
    
    ledHandle = PIN_open(&ledState, ledConfig);
    if(!ledHandle) {
       System_abort("Error initializing LED pins\n");
    }
    hMpuPin = PIN_open(&MpuPinState, MpuPinConfig);
    if (hMpuPin == NULL) {
    	System_abort("Pin open failed!");
    }
    buzzerHandle = PIN_open(&buzzerState, buzzerConfig);
    if(!buzzerHandle) {
       System_abort("Error initializing LED pins\n");
    }
    
	// JTKJ: Register the interrupt handler for the button
    // JTKJ: Rekister�i painonapille keskeytyksen k�sittelij�funktio

    if (PIN_registerIntCb(buttonHandle, &buttonFxn) != 0) {
       System_abort("Error registering button callback function");
    }
    if (PIN_registerIntCb(button1Handle, &button1Fxn) != 0) {
       System_abort("Error registering button1 callback function");
    }
    /* Sensor Task */
    Task_Params_init(&sensorTaskParams);
    sensorTaskParams.stackSize = STACKSIZE;
    sensorTaskParams.stack = &sensorTaskStack;
    sensorTaskParams.priority=2;
    
    sensorTask = Task_create(sensorTaskFxn, &sensorTaskParams, NULL);
    if (sensorTask == NULL) {
        System_abort("sensorTask create failed!");
    }

    /* Communication Task */
    Init6LoWPAN(); // This function call before use!

    Task_Params_init(&commTaskParams);
    commTaskParams.stackSize = STACKSIZE;
    commTaskParams.stack = &commTaskStack;
    commTaskParams.priority=1;

    commTask = Task_create(commTaskFxn, &commTaskParams, NULL);
    if (commTask == NULL) {
    	System_abort("commTask create failed!");
    }
    
    /* Draw Task */
    Task_Params_init(&drawTaskParams);
    drawTaskParams.stackSize = STACKSIZE;
    drawTaskParams.stack = &drawTaskStack;
    drawTaskParams.priority=2;
    
    drawTask = Task_create(drawTaskFxn, &drawTaskParams, NULL);
    if (drawTask == NULL) {
        System_abort("drawTask create failed!");
    }
    
    /* Buzz Task */
    Task_Params_init(&buzzTaskParams);
    buzzTaskParams.stackSize = STACKSIZE;
    buzzTaskParams.stack = &buzzTaskStack;
    buzzTaskParams.priority=2;
    
    buzzTask = Task_create(buzzTaskFxn, &buzzTaskParams, NULL);
    if (buzzTask == NULL) {
        System_abort("drawTask create failed!");
    }
    
    /* Start BIOS */
    BIOS_start();

    return (0);
}

float euclidean_distance(float Px, float Py, float Pz) {

    float ed = 0;
    ed = (float) sqrt( (pow(Px, 2) + pow(Py, 2) + pow(Pz, 2)) );
    
    return ed;
}

void identify_gesture(float ax, float ay, float az) {
    
    // HIGHFIVE BASELINE @ POSITION
    // X = 1.05
    // Y = -0.28
    // Z = 0.05
    
    // HANDSHAKE BASELINE @ POSITION
    // X = 0.05
    // Y = 0.90
    // Z = -0.10
    
    float x_hf_tresh = 1;
    float y_hf_tresh = 0.5;
    float z_hf_tresh = -0.8;
    
    float x_hs_tresh = -0.4;
    float y_hs_tresh = 1.1;
    float z_hs_tresh= -1.0;
    
    float x[20];
    float y[20];
    
    char dAc[80];
    
    if ((az <= -0.7 || az >= 0.7) && (ax >= 0.4 && ay <= 0.6)) {
        sprintf(dAc, "\n-HIGHFIVE-\nx: %.2f\ny: %.2f\nz: %.2f",  ax, ay, az);
        System_printf(dAc);
        System_flush(); 
        currState = HIGH_FIVE;
    }
    else if (ax < 0.4 && ay > 0.9 && az > -0.7 && az < 0.7) {
        sprintf(dAc, "\n-HANDSHAKE-\nx: %.2f\ny: %.2f\nz: %.2f",  ax, ay, az);
        System_printf(dAc);
        System_flush(); 
        currState = HANDSHAKE;
    }
    
    /*
    if (ax < x_hf_tresh && az < z_hf_tresh) {
        sprintf(dAc, "\n-HIGHFIVE-\nx: %.2f\ny: %.2f\nz: %.2f",  ax, ay, az);
        System_printf(dAc);
        System_flush(); 
        currState = HIGH_FIVE;
    }
    else if (ax < x_hs_tresh && ay > y_hs_tresh) {
        sprintf(dAc, "\n-HANDSHAKE-\nx: %.2f\ny: %.2f\nz: %.2f",  ax, ay, az);
        System_printf(dAc);
        System_flush(); 
        currState = HANDSHAKE;
    }
    */
}

void queue_message(char *msg) {
    
    strcpy(MSG_QUEUE[2], MSG_QUEUE[1]);
    strcpy(MSG_QUEUE[1], MSG_QUEUE[0]);
    strcpy(MSG_QUEUE[0], msg);
    
    // Kolme viestiä näytölle 
    sprintf(RCVD_MSG[0], "R#1: %s", MSG_QUEUE[0]);
    sprintf(RCVD_MSG[1], "R#2: %s", MSG_QUEUE[1]);
    sprintf(RCVD_MSG[2], "R#3: %s", MSG_QUEUE[2]);
    
}

void send_gesture_i2c(char *msg){
    
    BTN0_PRESSED = FALSE;
    
    char str[80];
    // char payload[16];
    // sprintf(payload, "%s", msg);
    // sprintf(dMsgOut, "%s", msg);
    sprintf(str,"\nSent message: %s", msg);
    System_printf(str);
    System_flush();
    
    //Send6LoWPAN(IEEE80154_SERVER_ADDR, payload, strlen(payload));
    Send6LoWPAN(ADDRESS, msg, strlen(msg));
    int32_t result = StartReceive6LoWPAN();
}

void change_mode() {
    
    currMode++;
    if (currMode > 2) {
        currMode = START;
    }
}

void set_transmit_mode() {
    
    BTN0_PRESSED = FALSE;
    
    if (currTransmitMode == SERVER) {
        currTransmitMode = BROADCAST;
        sprintf(dTransmitMode, "%s", "BROADCAST:");
        sprintf(dMsgOut, "N/A");
        ADDRESS = IEEE80154_BROADCAST;
        
    }
    else {
        currTransmitMode = SERVER;
        sprintf(dTransmitMode, "%s", "@SERVER:");
        sprintf(dMsgOut, "N/A");
        ADDRESS = IEEE80154_SERVER_ADDR;
    }
}

void buzz_receive_msg() {
    
    buzzerOpen(buzzerHandle);
    
    buzz(1800, 150);
    buzz(0, 100);
    buzz(1800, 150);
    buzz(0, 100);
    
    buzzerClose();
    
    //buzzerSetFrequency(1800);
    //Task_sleep(200000 / Clock_tickPeriod);
    //buzzerClose();
    //Task_sleep(250000 / Clock_tickPeriod);
    //buzzerOpen(buzzerHandle);
    //buzzerSetFrequency(1800);
    //Task_sleep(250000 / Clock_tickPeriod);
}

void buzz_noah2() {
    
    buzzerOpen(buzzerHandle);
    buzzerSetFrequency(1200);
    Task_sleep(200000 / Clock_tickPeriod);
    buzzerClose();
    Task_sleep(200000 / Clock_tickPeriod);
    
    buzzerOpen(buzzerHandle);
    buzzerSetFrequency(1200);
    Task_sleep(200000 / Clock_tickPeriod);
    buzzerClose();
    Task_sleep(200000 / Clock_tickPeriod);
    
    buzzerOpen(buzzerHandle);
    buzzerSetFrequency(1200);
    Task_sleep(200000 / Clock_tickPeriod);
    buzzerClose();
    Task_sleep(200000 / Clock_tickPeriod);
    
    buzzerOpen(buzzerHandle);
    buzzerSetFrequency(1400);
    Task_sleep(400000 / Clock_tickPeriod);
    buzzerClose();
    
    buzzerOpen(buzzerHandle);
    buzzerSetFrequency(1300);
    Task_sleep(200000 / Clock_tickPeriod);
    buzzerClose();
    Task_sleep(200000 / Clock_tickPeriod);
    
    buzzerOpen(buzzerHandle);
    buzzerSetFrequency(1300);
    Task_sleep(200000 / Clock_tickPeriod);
    buzzerClose();
    Task_sleep(200000 / Clock_tickPeriod);
    
    buzzerOpen(buzzerHandle);
    buzzerSetFrequency(1300);
    Task_sleep(200000 / Clock_tickPeriod);
    buzzerClose();
    Task_sleep(200000 / Clock_tickPeriod);
    
    buzzerOpen(buzzerHandle);
    Task_sleep(200000 / Clock_tickPeriod);
    buzzerSetFrequency(1500);
    Task_sleep(400000 / Clock_tickPeriod);
    buzzerClose();
    
    buzzerOpen(buzzerHandle);
    buzzerSetFrequency(1400);
    Task_sleep(200000 / Clock_tickPeriod);
    buzzerClose();
    Task_sleep(200000 / Clock_tickPeriod);
    
    buzzerOpen(buzzerHandle);
    buzzerSetFrequency(1400);
    Task_sleep(200000 / Clock_tickPeriod);
    buzzerClose();
    Task_sleep(200000 / Clock_tickPeriod);
    
    buzzerOpen(buzzerHandle);
    buzzerSetFrequency(1300);
    Task_sleep(200000 / Clock_tickPeriod);
    buzzerClose();
    Task_sleep(200000 / Clock_tickPeriod);
    
    buzzerOpen(buzzerHandle);
    buzzerSetFrequency(1300);
    Task_sleep(200000 / Clock_tickPeriod);
    buzzerClose();
    Task_sleep(200000 / Clock_tickPeriod);
    
    buzzerOpen(buzzerHandle);
    buzzerSetFrequency(1200);
    Task_sleep(400000 / Clock_tickPeriod);
    buzzerClose();
}

void buzz_noah(bool fp) {
    
    bool FIRST_PLAY;
    FIRST_PLAY = fp;
    
    buzzerOpen(buzzerHandle);
    
    buzz(1300, 200);
    buzz(0, 200);
    buzz(1300, 200);
    buzz(0, 200);
    buzz(1300, 200);
    buzz(0, 200);
    buzz(1500, 150);
    buzz(0, 200);
    
    buzz(1400, 200);
    buzz(0, 200);
    buzz(1400, 200);
    buzz(0, 200);
    buzz(1400, 200);
    buzz(0, 200);
    buzz(1600, 150);
    buzz(0, 200);
    
    buzz(1500, 200);
    buzz(0, 200);
    buzz(1500, 200);
    buzz(0, 200);
    buzz(1400, 200);
    buzz(0, 200);
    buzz(1400, 200);
    buzz(0, 200);
    buzz(1300, 400);
    buzz(0, 200);
    
    if (FIRST_PLAY) {
        buzz(1500, 200);
        buzz(0, 200);
        buzz(1500, 200);
        buzz(0, 200);
        buzz(1500, 200);
        buzz(0, 200);
        buzz(1500, 200);
        buzz(0, 200);
        buzz(1700, 400);
        buzz(0, 200);
        buzz(1600, 400);
        buzz(0, 200);    
        
        buzz(1400, 200);
        buzz(0, 200);
        buzz(1400, 200);
        buzz(0, 200);
        buzz(1400, 200);
        buzz(0, 200);
        buzz(1400, 200);
        buzz(0, 200);
        buzz(1600, 400);
        buzz(0, 200);
        buzz(1500, 400);
        buzz(0, 200);
        
        buzz_noah(false);
    }
    
    buzzerClose();
}

void buzz(int freq, int sleep_in_milliseconds) {
    
    int sleep;
    sleep = sleep_in_milliseconds * 1000;
    
    buzzerSetFrequency(freq);
    Task_sleep(sleep / Clock_tickPeriod);
    
}
