## Tietokonejärjestelmät harjoitustyö

Texas Instruments CC2650 wireless MCU -laitteen ohjelmointi.

1. Lämpötilan, ilmanpaineen, valoisuuden mittaus
2. Laitteiden välinen viestintä (6LowPAN)
3. Eleentunnistus (High-5, kädenpuristus)
4. Grafiikkaa
5. Ääni, musiikki (buzzer)
